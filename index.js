let students = [];

function addStudent(student){
	let studentsList = students.push(student);
	console.log(student + " was added to the student's list.");
}

function countStudents(){
	console.log("There are total of " + students.length + " students enrolled.");
}

function printStudents(){
	students.sort().forEach(function(student){
		console.log(student);
	})
}

function findStudent(filteredStudent){
	let filterStudentsList = students.filter(function(student){
		return student.toLowerCase().includes(filteredStudent.toLowerCase());
	})

	if (filterStudentsList.length === 0){
		console.log(filteredStudent + " is not an enrollee.");
	}

	else if (filterStudentsList.length === 1){
		console.log(filteredStudent + " is an enrollee.");
	}
	else{
		console.log(filterStudentsList + " are enrollees.");
	}
}